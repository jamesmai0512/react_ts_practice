import { useState } from 'react';
import { Routes, Route, Link} from 'react-router-dom'
import About from './exercise/useContext/about';
import Home from './exercise/useContext/home';
import { UserContext } from './exercise/useContext/useContext';

function App() {

  const [value, setValue] = useState('')
  return (
    <div>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/about">About</Link>
          </li>
        </ul>
      </nav>
      <UserContext.Provider value={{value, setValue}}>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/about' element={<About />} />
        </Routes>
      </UserContext.Provider>
    </div>
  )
}

export default App
