interface Props {
  code: number,
  title: string,
  detail: string
}

export function Data({code, title, detail}: Props) {
  return (
    <div>
      <h1>Here is the code {code}</h1>
      <p>Here is the title {title}</p>
      <h3>Here is the detail {detail}</h3>
    </div>
  )
}
