import React from 'react'
import {IProduct} from '../base/index'
import ProductTable from './ProductTable';
import SearchBar from './SearchBar';

interface FilterableProductTableProps {
  products: IProduct[]
}
 
interface FilterableProductTableState {
  
}
 
class FilterableProductTable extends React.Component<FilterableProductTableProps, FilterableProductTableState> {
  constructor(props: FilterableProductTableProps) {
    super(props);
    // this.state = { :  };
  }
  render() { 
    return ( 
      <div>
        <SearchBar/>
        <ProductTable products={this.props.products} />
      </div>
    );
  }
}
 
export default FilterableProductTable;