import React from "react";

interface ProductCategoryRowProps {
  category: string
}
 

class ProductCategoryRow extends React.Component<ProductCategoryRowProps> {
  constructor(props: ProductCategoryRowProps) {
    super(props);
  }
  render() { 
    const categrory = this.props.category
    return ( 
      <tr>
        <th colSpan={2}>
          {categrory}
        </th>
      </tr>
    );
  }
}
 
export default ProductCategoryRow;