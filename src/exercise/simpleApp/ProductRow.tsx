import React from "react";
import {IProduct} from '../base/index'

interface ProductRowProps {
  product: IProduct
}
 
interface ProductRowState {
  
}
 
class ProductRow extends React.Component<ProductRowProps, ProductRowState> {
  constructor(props: ProductRowProps) {
    super(props);
    // this.state = { :  };
  }
  render() { 
    const product: IProduct = this.props.product;
    const name: string | HTMLSpanElement = product.stocked ? product.name : 
    <span style={{color: 'red'}}>{product.name}</span>
    return ( 
      <tr>
        <td>{name}</td>
        <td>{product.price}</td>
      </tr>
     );
  }
}
 
export default ProductRow;