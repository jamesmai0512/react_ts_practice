import React from "react";
import {IProduct} from '../base/index'
import ProductCategoryRow from "./ProductCategoryRow";
import ProductRow from "./ProductRow";

interface ProductTableProps {
  products: IProduct[],
}
 
interface ProductTableState {
  
}
 
class ProductTable extends React.Component<ProductTableProps, ProductTableState> {
  constructor(props: ProductTableProps) {
    super(props);
    // this.state = { :  };
  }
  render() { 
    const rows: [] = [];
    let lastCategory: null = null;

    this.props.products.forEach((product: IProduct) => {
      if (product.category !== lastCategory) {
        <ProductCategoryRow 
        category={product.category} key={product.category} />
        rows.push(
        )
      }
      <ProductRow product={product} key={product.name}/>
      rows.push(
      )
    })
    
    return ( 
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
     );
  }
}
 
export default ProductTable;
