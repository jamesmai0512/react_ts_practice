interface IFormatDate {
  date: Date
}

export function FormatDate({date}: IFormatDate) {
  return (
    <h2>It is {date.toLocaleTimeString()}</h2>
  )
}