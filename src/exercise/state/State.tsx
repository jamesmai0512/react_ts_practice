import React from "react";
import {FormatDate} from './FormatDate'

interface ClockProps {
}
 
interface ClockState {
  date: Date,
}


 
class Clock extends React.Component<ClockProps, ClockState> {
  timerId: number
  constructor(props: ClockProps) {
    super(props);
    this.timerId = 0;
    this.state = { date: new Date()};
  }

  componentDidMount() {
    this.timerId = window.setInterval(() => this.tick(), 1000)
  }
  componentWillUnmount() {
    clearInterval(this.timerId)
  }

  tick() {
    this.setState({
      date: new Date()
    })
  }

  render() { 
    return ( 
      <div>
        <h1>Hello World</h1>
        <FormatDate date={this.state.date}/>
      </div>
     );
  }
}
 
export default Clock;
