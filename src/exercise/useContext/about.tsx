import React, {useContext} from "react";
import {UserContext} from "./useContext"

const About: React.FunctionComponent<{}> = props => {
  const {value, setValue} = useContext(UserContext);
  return ( 
    <div>
      <h1>About</h1>
      <p>{value}</p>

      <button onClick={() => setValue("James")}>Change value to James</button>
    </div>
   );
}

export default About;