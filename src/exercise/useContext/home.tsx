import React, {useContext} from "react";
import {UserContext} from "./useContext"

const Home: React.FC<{}> = props => {
  
  const {value, setValue} = useContext(UserContext)
  return ( 
    <div>
      <h1>Home</h1>
      <p>{value}</p>
      <button onClick={() => setValue("Hey James")}>Click</button>
    </div>
   );
}

export default Home;