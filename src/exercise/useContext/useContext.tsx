 import { createContext } from 'react';

interface IContext {
  value: string | undefined,
  setValue: React.Dispatch<React.SetStateAction<string>>
}
 
 export const UserContext = createContext<IContext>({} as IContext);