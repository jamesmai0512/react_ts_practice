import { useState, useEffect } from 'react'

interface IPost {
  userId: number,
  id: number,
  title: string,
  name: string,
  body: string
}
function Content() {
  const tags: string[]= ["posts", "comments", 'todos']
  const [posts, setPosts] = useState<IPost[]>([])
  const [type, setType] = useState('posts')

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/${type}`)
    .then(response => response.json())
    .then(posts => setPosts(posts))
  }, [type])
  
  return ( 
    <div>
      <div>
        {tags.map((tag: string) => (
          <button style={type === tag ? {backgroundColor: 'green'} : {}} key={tag} onClick={() => setType(tag)}>
            {tag}
          </button>
        ))}
      </div>
      
      <ul>
        {posts.map(post => (
          <li key={post.id}>{post.title || post.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default Content;