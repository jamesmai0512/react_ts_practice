import React, { useState, useEffect } from 'react'
import Content from './Content';
function Toggle() {
  const [show, setShow] = useState<boolean>(false)
  return ( 
    <div>
      <button onClick={() => setShow(!show)}>
        Toggle
      </button>
      {show && <Content/>}
    </div> 
  );
}

export default Toggle;