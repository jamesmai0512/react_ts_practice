import { useEffect, useRef, useState, MouseEvent } from "react";

function Clock() {
  const [count, setCount] = useState<number>(60)

  let timeId = useRef(60)
  useEffect(() => {
    
  })
  const handleStart = (e: MouseEvent) => {
    e.preventDefault();
    timeId.current = window.setInterval(()=>{
      setCount(preCount => preCount - 1)
    }, 1000)
   }

  const handleStop = () => {
    clearInterval(timeId.current)
  }
  return ( 
    <div>
      <h1>{count}</h1>
      <button onClick={handleStart}>Start</button>
      <button onClick={handleStop}>Stop</button>
    </div>
   );
}

export default Clock;