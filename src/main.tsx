import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router} from 'react-router-dom'
import App from './App'
import dataItem from './exercise/prop/newComponent'
import { Data } from './exercise/prop/propsComponent'
import FilterableProductTable from './exercise/simpleApp/FilterableProductTable'
import Toggle from './exercise/useEffect/Toggle'
import {IProduct} from './exercise/base/index'
import Clock from './exercise/useRef/Clock'

import './index.css'
// import App from './App'
// First example
    // const name :string= "James"
    // const element = <h1>Hello {name}</h1>

// Second example of sending params
    // interface User {
    //   firstName: string,
    //   lastName: string
    // }  

    // function formatName(user: User) {
    //   return user.firstName + ' ' + user.lastName;
    // }
    // const user: User = {
    //   firstName: "James",
    //   lastName: "Mai"
    // }
    
    // const element = <h1>Hello {formatName(user)}</h1>
    
    // <React.StrictMode>
    //   <App />
    // </React.StrictMode>,

  // function tick() {
  //   const element = ( // updating rendered element
  //     <div>
  //       <h1>Hello World</h1>
  //       <h2>It is {new Date().toLocaleTimeString()}</h2>
  //     </div>
  //   )
  //   ReactDOM.render(element ,document.getElementById('root'))
  // }

  // setInterval(tick, 1000)
  

const PRODUCT: IProduct[] = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
]
    
ReactDOM.render(
  // dataItem.map(data => (
  //   <Data
  //     code = {data.code}
  //     title = {data.title}
  //     detail = {data.detail}
  //     />
  // ))
  // <div>
  //   <Clock/>,
  //   <Clock />,
    // <Clock />,
  // </div>,
//<FilterableProductTable products={PRODUCT}/>, 
  // <Clock />,
  
  <Router>
    <App/>
  </Router>,
 document.getElementById("root"))